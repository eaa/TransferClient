<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <title>车间要料</title>

    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/bootstrap-theme.css" rel="stylesheet">
    <link href="../css/task_custom.css" rel="stylesheet">


</head>
<body>
<div class="container-fluid">
    <%
        if (session.getAttribute("username") == null) {
    %>
    <h1>未登录！</h1>
    3秒后跳转到登录页面
    <p>
        如果没有跳转，请点<a href="pages/Login.jsp">这里</a>
    </p>
    <%
            response.setHeader("refresh", "3;URL=pages/Login.jsp");
            return;
        }
    %>
    <div class="span10">
        <div class="theme-buy">
            <a class="btn btn-primary btn-large theme-login" href="javascript:;" id="maskin" style="display: none">弹出遮罩</a>
        </div>
        <div class="theme-popover">
            <div class="theme-poptit">
                <a href="javascript:void(0);" title="关闭" class="close">×</a>
                <h4 class="text-center">自制备料计划</h4>
            </div>
            <div class="theme-popbod dform"></div>
        </div>
        <div class="theme-popover-mask"></div>
    </div>
    <div class="row-fluid">
        <div class="span10">
            <div class="row-fluid">
                <div class="span10">
                    <div class="tabbable" id="tabs-238921">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#panel-720674" data-toggle="tab" id="reqmattab">任务需求</a>
                            </li>
                            <li>
                                <a href="#panel-547607" data-toggle="tab" id="donetasktab">已完成任务</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="panel-720674">
                                <table class="table table-bordered" id="task">
                                    <thead>
                                    <tr>
                                        <th class="task_checkbox"></th>
                                        <th>机台号</th>
                                        <th>物料名称</th>
                                        <th>规格 </th>
                                        <th>型号</th>
                                        <th>批次号</th>
                                        <th>需求重量</th>
                                        <th>开工时间</th>
                                        <th>状态</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="task_checkbox"><input type='checkbox' value=''/></td>
                                        <td></td>
                                        <td class="matname"></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane" id="panel-547607">
                                <table class="table table-bordered" id="done_task">
                                    <thead>
                                    <tr>
                                        <th class="task_checkbox"></th>
                                        <th>机台号</th>
                                        <th>物料名称</th>
                                        <th>规格</th>
                                        <th>型号</th>
                                        <th>批次号</th>
                                        <th>需求重量</th>
                                        <th>开工时间</th>
                                        <th>状态</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="task_checkbox">
                                            <input type='checkbox' value=''/>
                                        </td>
                                        <td></td>
                                        <td class="matname"></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <h4 class="text-center" id="reqtext">
                要料申请表
            </h4>
            <div class="row-fluid">
                <div class="span10">
                    <table class="table table-bordered" id="req">
                        <thead>
                        <tr>
                            <th></th>
                            <th>物料名称</th>
                            <th>规格</th>
                            <th>型号</th>
                            <th>批次号</th>
                            <th>需求重量</th>
                            <th>送达时限</th>
                            <th>已转库重量</th>
                            <th>状态</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr id='addRow'>
                            <td class="task_checkbox">
                                <button class="btn btn-success btn-mini" id="addreqpickm" type="button" onclick="rowAddClick()">+</button>
                            </td>
                            <td class="matname"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class="reqtime"></td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row-fluid" id="reqbtn">
                <button class="btn btn-info btn-mini" type="button" id="reqpickm">备料申请</button>
                <button class="btn btn-info btn-mini" type="button" id="matconfirm">领料确认</button>
                <button class="btn btn-info btn-mini" type="button" id="rtnmat" >要料完工</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="../js/jquery-2.1.0.js"></script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/task/task.js"></script>
<script type="text/javascript" src="../js/task/reqpickm.js"></script>
<script type="text/javascript" src="../js/task/work_connect.js"></script>
</body>
</html>