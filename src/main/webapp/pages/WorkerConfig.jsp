<%--
  Created by IntelliJ IDEA.
  User: yijx
  Date: 2017/3/27
  Time: 14:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport"
          content="width = device-width, initial-scale = 1.0, minimum-scale = 1.0, maximum-scale = 1.0, user-scalable = no"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/bootstrap-theme.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/config.css"/>

    <title>配置界面</title>
</head>


<body>
<div class="header">服务配置</div>
<%
    if (session.getAttribute("username") == null) {
%>
<h1>未登录！</h1>
3秒后跳转到登录页面
<p>
    如果没有跳转，请点<a href="pages/Login.jsp">这里</a>
</p>
<%
        response.setHeader("refresh", "3;URL=pages/Login.jsp");
        return;
    }
%>
<div class="top"></div>
<div class="section">
    <div class="property">
        <div class="input-group" style="float: left; width: 49%">
            <span class="input-group-addon ">用户：</span>
            <input id="username" name="username" type="text" class="form-control" value="${config.username}"
                   disabled="true">
        </div>
        <div class="input-group" style="float: right; width: 49%">
            <span class="input-group-addon ">组织：</span>
            <select disabled="true" id="org" name="position" type="text" class="form-control">
            </select>
            <span class="input-group-btn"><button class="btn btn-default" type="button" id="searchOrg" disabled="true">查询</button></span>
        </div>
    </div>
    <div class="property">
        <div class="input-group" style="float: left; width: 49%">
            <span class="input-group-addon ">仓库：</span>
            <select id="warehouse" name="warehouse" type="text" class="form-control" disabled="true">
            </select>
            <span class="input-group-btn"><button class="btn btn-default" type="button" id="searchWareHouse" disabled="true">查询</button></span>
        </div>
        <div class="input-group" style="float: right; width: 49%">
            <span class="input-group-addon ">站点：</span>
            <select disabled="true" id="position" name="position" type="text" class="form-control">
            </select>
            <span class="input-group-btn"><button class="btn btn-default" type="button" id="searchPosition" disabled="true">查询</button></span>
        </div>
    </div>
    <button id="linkNC" onclick="confirm()">确认</button>
    <button id="save" onclick="save()" style="visibility: hidden;">保存</button>
    <button id="edit" onclick="edit()">修改</button>
    <div class="property" style="background-color: #84C329; height: 30px">
    </div>
</div>

<script type="text/javascript" src="/js/jquery-2.1.0.js"></script>
<script type="text/javascript" src="/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/js/config/worker_config.js"></script>

</body>

</html>