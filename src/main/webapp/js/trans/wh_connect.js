/**
 * Created by yijx on 2017/4/11.
 */

//刷新备料申请列表请求
function refreshRequest() {
    $.ajax({
        type: "post",
        url: "../refreshList",
        data: "",
        dataType: "json",
        timeout: 30000,
        beforesend: function (XMLHttpRequest) {
        },
        success: function (msg) {
            constructList(msg);
        },
        erroe: function (msg) {
            alert(msg);
        }
    });
}


//跳转到转库单界面保存存数据到Session请求
function toTransBillRequest(transInfo) {
    $.ajax({
        type: "post",
        url: "../toTransBill",
        traditional: true,
        data: {info: JSON.stringify(transInfo)},
        dataType: "json",
        timeout: 30000,
        beforesend: function (XMLHttpRequest) {
        },
        success: function (msg) {
            toTransPage(msg);
        },
        error: function (msg) {
            alert(msg);
        }
    });
}