/**
 * Created by yijx on 2017/4/1.
 */

//保存转库单请求
function saveBillRequst(info) {
    $.ajax({
        type:"post",
        url:"../saveTransBill",
        data:{info:JSON.stringify(info)},
        traditional:true,
        dataType:"json",
        timeout:30000,
        berforesend:function (XMLHttpRequest) {},
        success:function (msg) {
            billSaved(msg);
        }
    });
}

//查询货位请求
function searchAllocation() {
    $.ajax({
        type: "post",
        url: "../getAllocation",
        data: "",
        dataType: "json",
        timeout: 30000,
        beforesend: function (XMLHttpRequest) {
        },
        success: function (msg) {
            gotAllocation(msg);
        }
    });
}

//查询已发车转库单
function queryDoneTransBill(info) {
    $.ajax({
        type: "post",
        url: "../getDoneTransBill",
        data: {info:JSON.stringify(info)},
        dataType: "json",
        timeout: 30000,
        beforesend: function (XMLHttpRequest) {
        },
        success: function (msg) {
            gotDoneTransBill(msg);
        }
    });
}