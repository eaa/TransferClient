/**
 * Created by yijx on 2017/3/31.
 */

var reqPickms;
//当前选中备料申请
var selectedReqPickm = {
    inwarehouse: "",
    position: "",
    selectRows: new Array()
};

function adaptTH() {
    $("table").children("thead").find("th").each(function () {
        var idx = $(this).index();
        var td = $(this).closest("table").children("tbody").children("tr:first").children("td").eq(idx);
        $(this).width(td.width());
    })
};

$(function () {
    adaptTH();
    refreshList();
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        e.target; // 激活的标签页
        e.relatedTarget; // 前一个激活的标签页
        adaptTH();
    })
});

//刷新列表
function refreshList() {
    refreshRequest();
}

//根据查询到的数据构造备料申请列表
function constructList(msg) {
    var rows = msg.rows;
    if (null == rows || rows.length == 0) {
        return;
    }
    reqPickms = rows;
    //清空
    var tbody = $("#req tbody");
    tbody.empty();
    //重新构造列表

    var result = "";
    for (var x in rows) {
        if (x % 2 == 1) {
            result += "<tr class='success' "
        } else {
            result += "<tr "
        }
        result += " onclick='rowClick(this)'>";
        result += "<td class='req_checkbox'>" +
            "<div class='billid' style='display: none'>" + rows[x].billid + "</div>" +
            "<div class='rowno' style='display: none'>" + rows[x].rowno + "</div>" +
            "<div class='reqwh' style='display: none'>" + rows[x].cinstockid + "</div>" +
            "<input type='checkbox'  disabled='disabled' value=''/></td>";
        result += "<td class='reqtime'>" + rows[x].reqTime + "</td>";
        result += "<td class='position'>" + rows[x].position + "</td>";
        result += "<td class='matname'>" + rows[x].matname + " / " + rows[x].matspec + " / " + rows[x].mattype + "</td>";
        result += "<td class='matBatch'>" + rows[x].matBatch + "</td>";
        result += "<td class='reqnum'><div class='nnum'>" + rows[x].reqNum + "</div><div class='matunit'>" + rows[x].matunit + "</div></td>";
        result += "<td class='donenum'><div class='nnum'>" + rows[x].nacctransnum + "</div><div class='matunit'>" + rows[x].matunit + "</div></td>";
        result += "<td class='status'>" + rows[x].status + "</td>";
        result += "</tr>"
    }
    tbody.append(result);
    adaptTH();
}

//行点击处理
function rowClick(tablerow) {
    var col = tablerow.childNodes;
    var rowIndex = tablerow.rowIndex - 1;
    var checkbox = col[0];
    //点击行过滤，一次只能选中同一个仓库同一站点
    //如果没有其它选中行,存入仓库站点
    if (selectedReqPickm.inwarehouse == "" || selectedReqPickm.position == ""
        || selectedReqPickm.selectRows.length == 0) {
        selectedReqPickm.inwarehouse = checkbox.childNodes[2].innerText;
        selectedReqPickm.position = col[2].innerText;
        selectedReqPickm.selectRows.push(rowIndex);
        checkbox.lastChild.checked = true;
    } else {
        //如果点击行未被选中，则判断仓库站点是否一致
        if (!checkbox.lastChild.checked) {
            if( selectedReqPickm.inwarehouse == checkbox.lastChild.previousSibling.innerText &&
                selectedReqPickm.position == col[2].innerText){
                checkbox.lastChild.checked = true;
                selectedReqPickm.selectRows.push(rowIndex);
            }else{
                $("#hint").text("只能选择一个站点的备料申请！");
                setTimeout("$('#hint').text('')",3000);
            }
        }
        //如果点击行被选中，弃选该行
        else {
            checkbox.lastChild.checked = false;
            for (var i = 0; i < selectedReqPickm.selectRows.length; i++) {
                if (selectedReqPickm.selectRows[i] == rowIndex) {
                    selectedReqPickm.selectRows.splice(i, 1);
                    break;
                }
            }
            if(selectedReqPickm.selectRows.length==0){
                selectedReqPickm.inwarehouse="";
                selectedReqPickm.position="";
            }
        }

    }
}


//转到转库单界面
function toTransBill() {
    if ("" == selectedReqPickm.inwarehouse ||""==selectedReqPickm.position || selectedReqPickm.selectRows.length == 0) {
        $("#hint").text("请选择备料申请！");
        setTimeout("$('#hint').text('')",3000);
        return;
    }
    var transInfo = {
        position: selectedReqPickm.position,
        inwarehouse: selectedReqPickm.inwarehouse,
        rows: ""
    }

    var rows = new Array();

    var tbody = $("#req").children("tbody");
    for (var x in selectedReqPickm.selectRows) {
        var tr = tbody[0].children[selectedReqPickm.selectRows[x]];
        var info = {
            billid: tr.children[0].firstChild.innerText,
            rowno: tr.children[0].firstChild.nextSibling.innerText,
            matinfo: tr.children[3].innerText,
            matBatch: tr.children[4].innerText,
            reqnum: tr.children[5].firstChild.innerText,
            donenum: tr.children[6].firstChild.innerText,
            matunit: tr.children[5].lastChild.innerText
        }
        rows.push(info);
    }
    transInfo.rows = rows;
    toTransBillRequest(transInfo);
}

function toTransPage(msg) {
    if (msg.flag == 0) {
        selectedReqPickm.inwarehouse="";
        selectedReqPickm.position="";
        selectedReqPickm.selectRows.splice(0, selectedReqPickm.length);
        window.location.href = "../transBillPage";
    }
}