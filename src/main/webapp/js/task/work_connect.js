/**
 * Created by yijx on 2017/3/23.
 */



//向后台查询生产任务
function taskQuery() {
    $.ajax({
        type: "post",
        url: "../taskQuery",
        data:"",
        dataType: "json",
        timeout: 30000,
        beforeSend: function (XMLHttpRequest) {
        },
        success: function (msg) {
            constructTask(msg);
        },
        error: function(msg){
            reqPickmFailed();
        }
    });

}

//查询当日已完成生产任务
function doneTaskQuery() {

}

//查询进行中的备料申请
function queryReqPickm() {

}

//保存备料申请到后台
function saveReqPickm(reqPickmData) {
    //更新备料申请单据号字段
    $.ajax({
        type: "post",
        url: "../saveReqPickM",
        traditional:true,
        data:{data:JSON.stringify(reqPickmData)},
        dataType: "json",
        timeout: 30000,
        beforeSend: function (XMLHttpRequest) {
        },
        success: function (msg) {
            reqPickmSaved(msg)
        },
        error: function(msg){
            reqPickmFailed();
        }
    });

}

//备料计划完成，推后台更新数据
function doneReqPickm(rowNo, bill) {
    $.ajax({
        type: "post",
        url: "../doneReqPickm",
        data: bill,
        dataType: "json",
        timeout: 30000,
        beforeSend: function (XMLHttpRequest) {
        },
        success: function (msg) {
            reqPickmDoneOpr(msg,rowNo);
        },
        error: function(msg){
            reqPickmFailed();
        }
    });
}

//转库单领料确认，后台更新备料申请已转库数量，放空车，并推前台
function transConfirm(transBill) {
    $.ajax({
        type: "post",
        url: "../transConfirm",
        data: transBill,
        dataType: "json",
        timeout: 30000,
        beforeSend: function (XMLHttpRequest) {
        },
        success: function (msg) {
            transConfirmed(msg);
        }
    });
}

//根据物料编码向后台查询物料详情
function searchMatFromServer(matCode) {
    $.ajax({
        type: "post",
        url: "../searchMatFromServer",
        data: {"matCode":matCode},
        dataType: "json",
        timeout: 30000,
        beforeSend: function (XMLHttpRequest) {
        },
        success: function (msg) {
            searchMatHandler(msg);
        }
    });
}
