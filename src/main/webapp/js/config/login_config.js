/**
 * Created by yijx on 2017/4/6.
 */
$(function () {
    //从cookie中读取用户类型、账户、密码
    var usertype = $.cookie("usertype");
    var username =$.cookie("username");
    var password = $.cookie("password");
    var remember = $.cookie("remember");

    if(null!=usertype){
        $("#usertype").val(usertype);
    }
    if(null !=username){
        $("#username").val(username);
    }
    $("#password").val(password);
    $("#remember")[0].checked = remember;

    var login = $("#login");
    // 登录
    login.on('click', function () {
        usertype = $("#usertype").val();
        username = $("#username").val();
        password = $("#password").val();
        if (username == "" || password == "") {
            alert("请输入用户名密码！");
            return;
        }
        var userInfo = {
            usertype: usertype,
            username: username,
            password: password
        };
        $.ajax({
            type: "post",
            url: "../logining",
            data: userInfo,
            dataType: "json",
            timeout: 15000,
            beforeSend: function (XMLHttpRequest) {
            },
            success: function (dataMsg) {
                var flag = dataMsg.flag;
                if (flag == 0) {
                    $("#password").val(null);
                    alert("用户名或密码错误，登录失败！");
                } else {
                    //保存cookie
                    if($("#remember")[0].checked){
                        $.cookie("remember","true",{expires:7});
                        $.cookie("usertype",usertype,{expires:7});
                        $.cookie("username",username,{expires:7});
                        $.cookie("password",password,{expires:7});
                    }else{
                        $.cookie("remember","false",{expires:-1});
                        $.cookie("usertype","",{expires:-1});
                        $.cookie("username","",{expires:-1});
                        $.cookie("password","",{expires:-1});
                    }

                    // 界面跳转
                    if(usertype === "admin"){
                        window.location.href = "/LoadAdminConfig";
                    }else{
                        window.location.href = "/LoadWorkerConfig";
                    }
                }
            }
        });
    });
});
function selectChange() {
    $("#username").val("");
    $("#password").val("");
}
