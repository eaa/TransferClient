/**
 * Created by yijx on 2017/4/6.
 */
var propInfo;
var itemKey=["org","warehouse","position","searchOrg","searchWareHouse","searchPosition"];
$(function () {
    propInfo = {
        username: $('#username').val(),
        org:$.cookie("org")==null?"":$.cookie("org"),
        orgText: $.cookie("orgText")==null?"":$.cookie("orgText"),
        warehouse:$.cookie("warehouse")==null?"":$.cookie("warehouse"),
        warehouseText: $.cookie("warehouseText")==null?"":$.cookie("warehouseText"),
        position: $.cookie("position")==null?"":$.cookie("position"),
        positionText:$.cookie("positionText")==null?"":$.cookie("positionText")
    };
    var html = "<option value='"+propInfo.org+"'>"+propInfo.orgText+"</option>";
    $('#org').append(html);
    var html = "<option value='"+propInfo.warehouse+"'>"+propInfo.warehouseText+"</option>";
    $('#warehouse').append(html);
    html = "<option value='"+propInfo.position+"'>"+propInfo.positionText+"</option>";
    $('#position').append(html);

    //切换组织后，仓库站点置空
    $("#org").change(function () {
        $("#warehouse").html("");
        $("#warehouse").val("");
        $('#position').html("");
        $('#position').val("");
    });

    //切换仓库后，站点置空
    $("#warehouse").change(function () {
        $('#position').html("");
        $('#position').val("");
    });

    var whButton = $("#searchWareHouse"),
        orgButton=$("#searchOrg"),
        pstButton =$("#searchPosition");

    //查询组织按钮
    whButton.on('click',function () {
        $.ajax({
            type:"post",
            url:"searchOrg",
            data:"",
            dataType:"json",
            timeout:30000,
            beforesend:function (XMLHttpRequest) {
            },
            success:function (msg) {
                gotOrg(msg);
            }
        })
    });

    //查询仓库按钮
    whButton.on('click',function () {
        if($('#org').val()==""){
            alert("必须先选择组织！");
            return;
        }
        $.ajax({
            type:"post",
            url:"searchWarehouse",
            data:"",
            dataType:"json",
            timeout:30000,
            beforesend:function (XMLHttpRequest) {
            },
            success:function (msg) {
                gotWarehouse(msg);
            }
        })
    });
    //查询站点按钮
    pstButton.on('click',function () {
        if($('#warehouse').val()==""){
            alert("必须先选择仓库！");
            return;
        }
        $.ajax({
            type:"post",
            url:"searchPosition",
            data:"",
            dataType:"json",
            timeout:30000,
            beforesend:function (XMLHttpRequest) {
            },
            success:function (msg) {
                gotPosition(msg);
            }
        })
    });

});
// 保存
function save() {
    propInfo.username = $('#username').val();
    propInfo.org=$("#org").val();
    propInfo.warehouse=$('#warehouse').val();
    propInfo.position=$('#position').val();
    propInfo.orgText=$("#org").find("option:selected").text();
    propInfo.warehouseText = $('#warehouse').find("option:selected").text();
    propInfo.positionText = $('#position').find("option:selected").text();
    for (var key in propInfo) {
        if (propInfo[key] == "") {
            alert("配置信息中任意项不能为空！");
            return;
        }
    }
    $.ajax({
        type: "post",
        url: "positionToSession",
        data: propInfo,
        dataType: "json",
        timeout: 15000,
        beforeSend: function (XMLHttpRequest) {
        },
        success: function (dataMsg) {
            var flag = dataMsg.flag;
            if (flag == 0) {
                // 保存成功，设置所有input不能编辑
                for (var key in itemKey) {
                    $("#" + itemKey[key]).attr('disabled', true);
                }
                // 隐藏保存按钮放开编辑按钮
                document.getElementById("save").style.visibility = "hidden";
                document.getElementById("edit").style.visibility = "visible";
                $.cookie("org",propInfo.org,{expires:7});
                $.cookie("warehouse",propInfo.warehouse,{expires:7});
                $.cookie("position",propInfo.position,{expires:7});
                $.cookie("orgText",propInfo.orgText,{expires:7});
                $.cookie("warehouseText",propInfo.warehouseText,{expires:7});
                $.cookie("positionText",propInfo.positionText,{expires:7})
            }
        }
    });
}
// 确定信息，进入工作界面
function confirm() {
    for (var key in itemKey) {
        // 开始前先保存
        if (!$("#" + itemKey[key]).attr("disabled")) {
            alert("请先保存配置信息！");
            return;
        }
    }
    for (var key in propInfo) {
        if (propInfo[key] == "") {
            alert("配置信息中任意项不能为空！");
            return;
        }
    }

    $.ajax({
        type: "post",
        url: "positionToSession",
        data: propInfo,
        dataType: "json",
        timeout: 15000,
        beforeSend: function (XMLHttpRequest) {
        },
        success: function (dataMsg) {
            var flag = dataMsg.flag;
            if (flag == 0) {
                //各项配置信息存入cookie
                $.cookie("org",propInfo.org,{expires:7});
                $.cookie("warehouse",propInfo.warehouse,{expires:7});
                $.cookie("position",propInfo.position,{expires:7});
                $.cookie("orgText",propInfo.orgText,{expires:7});
                $.cookie("warehouseText",propInfo.warehouseText,{expires:7});
                $.cookie("positionText",propInfo.positionText,{expires:7});
                //跳转到线边仓终端或仓库终端
                if(dataMsg.usertype == "workshop"){
                    window.location.href = "/WorkTask";
                }else if(dataMsg.usertype =="warehouse"){
                    window.location.href = "/WareHouse";
                }

            }
        }
    });
}

// 修改
function edit() {
    for (var key in itemKey) {
        $("#" + itemKey[key]).attr("disabled",false);
    }
    // 隐藏编辑按钮放开保存按钮
    document.getElementById("save").style.visibility = "visible";
    document.getElementById("edit").style.visibility = "hidden";
}

//后台查询出仓库后界面响应
function gotWarehouse(msg) {
    if(null !=msg>0){
        var warehouse = $("#warehouse").empty();
        for(var id in msg){
            warehouse.append("<option value='"+id+"'>"+msg[id]+"</option>")
        }
    }
}

//后台查询出仓库后界面响应
function gotPosition(msg) {
    if(null !=msg){
        var warehouse = $("#position").empty();
        for(var id in msg){
            warehouse.append("<option value='"+id+"'>"+msg[id]+"</option>")
        }
    }
}

//后台查询出组织后界面响应
function gotOrg(msg) {
    if(null !=msg){
        var org = $("#org").empty();
        for(var id in msg){
            org.append("<option value='"+id+"'>"+msg[id]+"</option>")
        }
    }
}