/**
 * Created by yijx on 2017/4/6.
 */
var propInfo;
$(function () {
    propInfo = {
        ncip: $('#ncip').val(),
        ncdataresource: $('#ncdataresource').val(),
        ncusername: $('#ncusername').val(),
        ncuserpassword: $('#ncuserpassword').val(),
        ncversion:$('#ncversion').val()
    }
});
// 保存
function save() {
    propInfo.ncip = $('#ncip').val();
    propInfo.ncdataresource = $('#ncdataresource').val();
    propInfo.ncusername = $('#ncusername').val();
    propInfo.ncuserpassword = $('#ncuserpassword').val();
    propInfo.ncversion = $('#ncversion').val();

    for (var key in propInfo) {
        if (propInfo[key] == "") {
            alert("配置信息中任意项不能为空！");
            return;
        }
    }
    $
        .ajax({
            type: "post",
            url: "SaveConfig",
            data: propInfo,
            dataType: "json",
            timeout: 15000,
            beforeSend: function (XMLHttpRequest) {
            },
            success: function (dataMsg) {
                var flag = dataMsg.flag;
                if (flag == 0) {
                    // 保存成功，设置所有input不能编辑
                    for (var key in propInfo) {
                        $("#" + key).attr('readOnly', 'readOnly');
                    }
                    // 隐藏保存按钮放开编辑按钮
                    document.getElementById("save").style.visibility = "hidden";
                    document.getElementById("edit").style.visibility = "visible";
                }
            }
        });
}
// 测试连接
function linkNC() {
    for (var key in propInfo) {
        if ($("#" + key).attr("readOnly") == undefined) {
            alert("请先保存配置信息！");
            return;
        }
    }
    for (var key in propInfo) {
        if (propInfo[key] == "") {
            alert("配置信息中任意项不能为空！");
            return;
        }
    }
    // 测试连接前先保存
    $.ajax({
        type: "post",
        url: "LinkNC",
        dataType: "json",
        timeout: 15000,
        beforeSend: function (XMLHttpRequest) {
        },
        success: function (dataMsg) {
            var flag = dataMsg.flag;
            if (flag == 0) {
                alert("微协同服务器与NC服务器连接正常！");
            } else if (flag == 1) {
                alert("微协同服务器与NC服务器连接异常！请确认数据源、用户、密码等配置信息！");
            }
        }
    });
}

// 修改
function edit() {
    for (var key in propInfo) {
        $("#" + key).removeAttr("readOnly");
        // $("#" + key).attr('readOnly', 'readOnly');
    }
    // 隐藏编辑按钮放开保存按钮
    document.getElementById("save").style.visibility = "visible";
    document.getElementById("edit").style.visibility = "hidden";
}