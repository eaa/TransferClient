package com.yonyou.mm.aop;

import org.apache.log4j.Logger;
import org.apache.log4j.Priority;

/**
 * NC用户同步操作日志类
 * 
 * @date 2016年8月1日 下午1:57:34
 * @author wangjwm
 *
 */
public class LogUtil {

	private static Logger log = Logger.getLogger(LogUtil.class);


	/**
	 * AOP日志
	 *
	 *            执行前后/AROUND
	 * @param msg
	 *            日志信息
	 */
	public static void aopLog(String msg) {
		log.info(msg);
	}

	/**
	 * 异常日志
	 * 
	 * @param message
	 *            异常信息
	 */
	public static void exceptionLog(String message, Throwable e) {
		log.error(message, e);
	}

	/**
	 * 其他异常
	 * 
	 * @param level
	 *            异常类别 Level.ERROR\Level.INFO...
	 * @param message
	 *            异常信息
	 */
	public static void otherLog(Priority level, Object message) {
		log.log(level, message);
	}

}
