package com.yonyou.mm.aop;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Controller层日志注解
 * 
 * @date 2016年8月4日 上午11:32:43
 * @author wangjwm
 *
 */
@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface ControllerLog {

	String remark() default "";

}
