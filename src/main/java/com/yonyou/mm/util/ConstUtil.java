/**
 * 
 */
package com.yonyou.mm.util;

/**
 * 字符标记常量
 * 
 * @author suny
 */
public class ConstUtil {

	//交互信息通用key
	public static final String GROUP="pk_gourp";
	public static final String ORG="pk_org";
	public static final String USERTYPE="usertype";
	public static final String DBSOURCE="dbname";
	public static final String USERNAME="username";
	public static final String PASSWORD= "password";
	public static final String TASKTYPE ="tasktype";

	//备料申请交互信息key
	//行
	public static final String ROWS="rows";
	//表体
	public static final String POSITION = "position";
	public static final String MATCODE="matCode";
	public static final String MATBATCH="matBatch";
	public static final String REQNUM = "reqNum";
	public static final String REQTIME = "reqTime";

	//保存备料申请返回值
	public static final String ROWNO = "rowno";
	public static final String BILLID="billid";

	//仓库、站点key
	public static final String WAREHOUSE="warehouse";
	//组织、仓库、站点名称
	public static final String ORGNAME="orgText";
	public static final String WAREHOUSENAME="warehouseText";
	public static final String POSITIONNAME="positionText";

	//转库单
	public static final String TRANSBILLCODE="transBillCode";

	/*
	 * session 中用户ID对应键名
	 */
	public static final String KEY_USERID_IN_SESSION = "UserId";

	/*
	 * session 中用户类型对应键名
	 */
	public static final String KEY_TYPE_IN_SESSION = "type";

	/*
	 * 查询单据数据URL
	 */
	public static final String WORKTASK_URL = "/service/WorkTaskService";

}
