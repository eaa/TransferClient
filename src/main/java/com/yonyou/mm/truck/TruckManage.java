package com.yonyou.mm.truck;

/**
 * Created by yijx on 2017/3/24.
 */

/**
 * 与轨道物流系统交互
 */
public class TruckManage {

    /**
     * 叫空车
     */
    public void reqVoidTruck(){

    }

    /**
     * 发满车
     */
    public void sendFullTruck(){

    }

    /**
     * 放空车
     */
    public  void releaseVoidTruck(){

    }
}
