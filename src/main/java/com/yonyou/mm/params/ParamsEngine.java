package com.yonyou.mm.params;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.XMLConfiguration;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 初始化加载配置文件类
 *
 * @date 2016年8月1日 上午10:47:53
 * @author wangjwm
 *
 */
public class ParamsEngine implements ServletContextListener {

	// XML配置文件路径
	// public static final String xmlConfigPath = "nc/weixin/params/variableConfig.xml";
	public static final String xmlConfigPath = "variableConfig.xml";
	// XML配置文件
	private static Configuration xmlConfig;


	public static Configuration getXmlConfig() {
		return xmlConfig;
	}

	public static void setXmlConfig(Configuration xmlConfig) {
		ParamsEngine.xmlConfig = xmlConfig;
	}


	public void contextDestroyed(ServletContextEvent arg0) {
	}

	public void contextInitialized(ServletContextEvent arg0) {
		// 加载配置参数
		try {
			String str = this.getClass().getResource("/").getPath();
			str = str.substring(1, str.indexOf("classes"));
			Configuration config = new XMLConfiguration(str + ParamsEngine.xmlConfigPath);
			if (config != null) {
				ParamsEngine.setXmlConfig(config);
			}
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}
	}

}
