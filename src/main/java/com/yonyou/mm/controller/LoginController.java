package com.yonyou.mm.controller;

import com.yonyou.mm.aop.ControllerLog;
import com.yonyou.mm.aop.LogUtil;
import com.yonyou.mm.params.ParamsEngine;
import com.yonyou.mm.service.ServiceUtil;
import com.yonyou.mm.util.ConstUtil;
import com.yonyou.mm.util.TestData;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by yijx on 2017/3/23.
 */
@Controller
public class LoginController {

    @RequestMapping(value = "/")
    public String index() {
        return "Login";
    }

    /**
     * 管理员登录，配置NC地址、数据源等信息
     * @param request
     * @param response
     * @param paramData
     */
    @RequestMapping(value = "/logining")
    @ControllerLog(remark = "登录")
    public void adminLogin(HttpServletRequest request, HttpServletResponse response, @RequestParam HashMap<String, Object> paramData,Model model){
        HttpSession session = request.getSession();
        ServiceUtil configutilservice = new ServiceUtil();
        boolean isLogin = configutilservice.isLogin(paramData,session);
        JSONObject obj = new JSONObject();
        if (isLogin) {
            String  userType = (String) paramData.get(ConstUtil.USERTYPE);
            session.setAttribute(ConstUtil.USERTYPE,userType);
            session.setAttribute(ConstUtil.USERNAME, paramData.get(ConstUtil.USERNAME));
            session.setAttribute(ConstUtil.PASSWORD, paramData.get(ConstUtil.PASSWORD));
            String dbname = ParamsEngine.getXmlConfig().getString("TransferClient.ncdataresource");
            session.setAttribute(ConstUtil.DBSOURCE,dbname);
            obj.put("flag",1);
        } else {
            obj.put("flag", 0);
        }
        try {
            PrintWriter pw = response.getWriter();
            pw.write(obj.toString());
        } catch (IOException e) {
            LogUtil.exceptionLog(e.getMessage(), e);
        }
    }

    @RequestMapping(value = { "/SaveConfig" })
    @ControllerLog(remark = "保存系统配置")
    public void SaveConfig(HttpServletResponse response, @RequestParam Map<Object, Object> obj) {
        // 修改variableConfig.xml的配置文件
        ServiceUtil configutilservice = new ServiceUtil();
        configutilservice.saveConfig(obj);
        JSONObject objRet = new JSONObject();
        objRet.put("flag", 0);
        try {
            PrintWriter pw = response.getWriter();
            pw.write(objRet.toString());
        } catch (IOException e) {
            LogUtil.exceptionLog(e.getMessage(), e);
        }
    }

    /**
     * 将仓库信息存入Session
     * @param request
     * @param response
     */
    @RequestMapping(value = "/positionToSession")
    @ControllerLog(remark = "保存站点信息到Session")
    public void positionToSession(HttpServletRequest request,HttpServletResponse response){
        HttpSession session = request.getSession();
        String org = request.getParameter(ConstUtil.ORG);
        String warehouse = request.getParameter(ConstUtil.WAREHOUSE);
        String position= request.getParameter(ConstUtil.POSITION);
        String orgName = request.getParameter(ConstUtil.ORGNAME);
        String warehouseName=request.getParameter(ConstUtil.WAREHOUSENAME);
        String positionName=request.getParameter(ConstUtil.POSITIONNAME);
        session.setAttribute(ConstUtil.ORG,org);
        session.setAttribute(ConstUtil.WAREHOUSE,warehouse);
        session.setAttribute(ConstUtil.POSITION,position);
        session.setAttribute(ConstUtil.ORGNAME,orgName);
        session.setAttribute(ConstUtil.WAREHOUSENAME,warehouseName);
        session.setAttribute(ConstUtil.POSITIONNAME,positionName);
        JSONObject objRet = new JSONObject();
        objRet.put("flag", 0);
        objRet.put(ConstUtil.USERTYPE,session.getAttribute(ConstUtil.USERTYPE));
        try {
            PrintWriter pw = response.getWriter();
            pw.write(objRet.toString());
        } catch (IOException e) {
            LogUtil.exceptionLog(e.getMessage(), e);
        }
    }

    @RequestMapping(value = { "/LinkNC" })
    @ControllerLog(remark = "测试NC连接")
    public void LinkNC(HttpServletRequest request, HttpServletResponse response) {
        // 是否连通
        ServiceUtil configutilservice = new ServiceUtil();
        boolean isLinkNC = configutilservice.isLinkNC();
        JSONObject obj = new JSONObject();
        if (isLinkNC) {
            obj.put("flag", 0);
        } else {
            obj.put("flag", 1);
        }
        try {
            PrintWriter pw = response.getWriter();
            pw.write(obj.toString());
        } catch (IOException e) {
            LogUtil.exceptionLog(e.getMessage(), e);
        }
    }

    /**
     * 加载系统配置界面
     * @param request
     * @param model
     * @return
     */
    @RequestMapping(value = { "/LoadAdminConfig" })
    @ControllerLog(remark = "加载系统配置")
    public String LoadConfig(HttpServletRequest request, Model model) {
        ServiceUtil configutilservice = new ServiceUtil();
        model = configutilservice.addAdminAttr(model);
        return "AdminConfig";
    }

    /**
     * 加载车间配置界面
     * @param request
     * @param model
     * @return
     */
    @RequestMapping(value = "/LoadWorkerConfig")
    @ControllerLog(remark = "加载车间配置")
    public String LoadWorkTask(HttpServletRequest request, Model model){
        HttpSession session = request.getSession();
        HashMap info = new HashMap<String, Object>();
        info.put(ConstUtil.USERNAME,session.getAttribute(ConstUtil.USERNAME));

        ServiceUtil configutilservice = new ServiceUtil();
        String val  = configutilservice.addWorkTaskAttr(info);
        model.addAttribute("config", JSONObject.fromObject(val));
        return "WorkerConfig";
    }

    /**
     * 查询仓库
     * @param request
     * @param response
     */
    @RequestMapping(value = "/searchWarehouse")
    @ControllerLog(remark = "查询仓库")
    public void searchWH(HttpServletRequest request, HttpServletResponse response){
        HttpSession session = request.getSession();
        JSONObject info = new JSONObject();
        info.put(ConstUtil.TASKTYPE,"searchWarehouse");
        JSONObject result = ServiceUtil.requestService(info,session);
        JSONArray warehouses = new JSONArray();
        if(result.containsKey("data") && result.get("data") instanceof JSONArray) {
            warehouses= result.getJSONArray("data");
        }
        warehouses = JSONArray.fromObject(TestData.warehouses);
        try {
            PrintWriter pw = response.getWriter();
            pw.write(warehouses.toString());
        } catch (IOException e) {
            LogUtil.exceptionLog(e.getMessage(),e);
        }
    }

    /**
     * 查询站点
     * @param request
     * @param response
     */
    @RequestMapping(value = "/searchPosition")
    @ControllerLog(remark = "查询站点")
    public void searchPosition(HttpServletRequest request, HttpServletResponse response){
        HttpSession session = request.getSession();
        JSONObject info = new JSONObject();
        info.put(ConstUtil.TASKTYPE,"searchPosition");
        info.put(ConstUtil.WAREHOUSE,request.getParameter(ConstUtil.WAREHOUSE));
        JSONObject result = ServiceUtil.requestService(info,session);
        JSONArray positions = new JSONArray();
        if(result.containsKey("data") && result.get("data") instanceof JSONArray) {
            positions= result.getJSONArray("data");
        }
        positions = JSONArray.fromObject(TestData.positions);
        try {
            PrintWriter pw = response.getWriter();
            pw.write(positions.toString());
        } catch (IOException e) {
            LogUtil.exceptionLog(e.getMessage(),e);
        }
    }


    /**
     * 查询组织
     * @param request
     * @param response
     */
    @RequestMapping(value = "/searchOrg")
    @ControllerLog(remark = "查询组织")
    public void searchOrg(HttpServletRequest request, HttpServletResponse response){
        HttpSession session = request.getSession();
        JSONObject info = new JSONObject();
        info.put(ConstUtil.TASKTYPE,"searchOrg");
        JSONObject result = ServiceUtil.requestService(info,session);
        JSONArray orgs = new JSONArray();
        if(result.containsKey("data") && result.get("data") instanceof JSONArray) {
             orgs= result.getJSONArray("data");
        }
        orgs = JSONArray.fromObject(TestData.orgs);
        try {
            PrintWriter pw = response.getWriter();
            pw.write(orgs.toString());
        } catch (IOException e) {
            LogUtil.exceptionLog(e.getMessage(),e);
        }
    }

}
