package com.yonyou.mm.controller;

import com.yonyou.mm.aop.ControllerLog;
import com.yonyou.mm.aop.LogUtil;
import com.yonyou.mm.service.ServiceUtil;
import com.yonyou.mm.util.ConstUtil;
import com.yonyou.mm.util.TestData;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.log4j.Level;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

/**
 * Created by yijx on 2017/3/22.
 */
@Controller
public class WorkController {

    @RequestMapping(value = "/WorkTask")
    public String transferPage() {
        return "WorkTask";
    }

    @RequestMapping(value = "/WareHouse")
    public String wareHousePage() {
        return "WareHouse";
    }

    /**
     * 查询生产任务备料计划
     */
    @RequestMapping(value = "/taskQuery")
    @ControllerLog(remark = "查询任务")
    public void taskQuery(HttpServletRequest request, HttpServletResponse response) {
        JSONArray list = JSONArray.fromObject(TestData.taskList);
        JSONObject result = new JSONObject();
        result.put("tasks",list);
        try {
            PrintWriter pw = response.getWriter();
            pw.write(result.toString());
        } catch (IOException e) {
            LogUtil.exceptionLog(e.getMessage(),e);
        }

    }

    /**
     * 查询当日已完成生产任务备料计划
     *
     * @param request
     * @param response
     */
    @RequestMapping(value = "/doneTaskQuery")
    @ControllerLog(remark = "查询完工生产任务")
    public void doneTaskQuery(HttpServletRequest request, HttpServletResponse response) {

    }

    /**
     * 查询当日备料申请
     *
     * @param request
     * @param response
     */
    @RequestMapping(value = "/queryReqPickm")
    @ControllerLog(remark = "查询备料申请")
    public void queryReqPickm(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        //传后台数据
        JSONObject info = new JSONObject();
        //站点
        info.put(ConstUtil.POSITION, session.getAttribute(ConstUtil.POSITION));

        //请求类型为保存备料申请
        info.put(ConstUtil.TASKTYPE, "queryReqPickm");

        //回传数据
        JSONObject obj = ServiceUtil.requestService(info,session);
    }

    /**
     * 保存备料申请到后台
     *
     * @param request
     * @param response
     */
    @RequestMapping(value = {"/saveReqPickM"})
    @ControllerLog(remark = "保存备料申请")
    public void saveReqPickM(HttpServletRequest request, HttpServletResponse response, @RequestParam HashMap<String, Object> paramData) {
        HttpSession session = request.getSession();
        Object params = paramData.get("data");
        JSONArray jsonarray = JSONArray.fromObject(params);
        //传后台数据
        JSONObject info = new JSONObject();
        //请求类型为保存备料申请
        info.put(ConstUtil.TASKTYPE, "save");
        //备料申请行
        info.put(ConstUtil.ROWS, jsonarray);
        //站点
        info.put(ConstUtil.POSITION, session.getAttribute(ConstUtil.POSITION));
        //回传数据
        JSONObject obj = ServiceUtil.requestService(info,session);
        //请求后台处理

        try {
            PrintWriter pw = response.getWriter();
//            JSONArray vos = JSONArray.fromObject(TestData.reqPickmData);
//            pw.write(vos.toString());
            if (obj.containsKey("result")) {
                JSONArray vos = obj.getJSONArray("result");
                pw.write(vos.toString());
            } else if (obj.containsKey("error")) {
                LogUtil.otherLog(Level.ERROR,obj.get("error"));
                pw.write("error");
            }
        } catch (IOException e) {
            LogUtil.exceptionLog(e.getMessage(), e);
        }

    }

    /**
     * 备料申请完工
     *
     * @param request
     * @param response
     * @param paramData
     */
    @RequestMapping(value = "/doneReqPickm")
    @ControllerLog(remark = "备料申请完工")
    public void doneReqPickm(HttpServletRequest request, HttpServletResponse response, @RequestParam HashMap<String, Object> paramData) {
        HttpSession session = request.getSession();
        JSONObject info =new JSONObject();
        //单据id行号
        info.put(ConstUtil.BILLID,paramData.get(ConstUtil.BILLID));
        info.put(ConstUtil.ROWNO,paramData.get(ConstUtil.ROWNO));
        //请求类型为备料申请完成
        info.put(ConstUtil.TASKTYPE, "done");
        JSONObject result = ServiceUtil.requestService(info,session);
        JSONObject obj = new JSONObject();
        if (result.containsKey("success")) {
            obj.put("flag", 1);
        } else {
            if(result.containsKey("error")) {
                LogUtil.otherLog(Level.ERROR, result.get("error"));
            }
            obj.put("flag", 0);
        }
        try {
            PrintWriter pw = response.getWriter();
            pw.write(obj.toString());
        } catch (IOException e) {
            LogUtil.exceptionLog(e.getMessage(), e);
        }

    }

    /**
     * 领料确认
     *
     * @param request
     * @param response
     * @param paramData
     */
    @RequestMapping(value = "/transConfirm")
    @ControllerLog(remark = "领料确认")
    public void transConfirm(HttpServletRequest request, HttpServletResponse response, @RequestParam HashMap<String, Object> paramData) {
        HttpSession session = request.getSession();
        String transBill = request.getParameter(ConstUtil.TRANSBILLCODE);
        JSONObject info =new JSONObject();
        info.put(ConstUtil.TRANSBILLCODE,transBill);
        info.put(ConstUtil.TASKTYPE,"confirm");
        JSONObject result = ServiceUtil.requestService(info,session);

        JSONObject reqPickm = JSONObject.fromObject(TestData.reqPickm);
        try {
            PrintWriter pw = response.getWriter();
            pw.write(reqPickm.toString());
        } catch (IOException e) {
            LogUtil.exceptionLog(e.getMessage(), e);
        }
    }

    /**
     * 根据物料编码查询物料详细信息
     *
     * @param request
     * @param response
     * @param paramData
     */
    @RequestMapping(value = "/searchMatFromServer")
    @ControllerLog(remark = "查询物料详细信息")
    public void searchMatFromServer(HttpServletRequest request, HttpServletResponse response, @RequestParam HashMap<String, Object> paramData) {
        HttpSession session = request.getSession();
        JSONObject info =new JSONObject();
        //请求类型为查询物料信息
        info.put(ConstUtil.TASKTYPE, "searchMat");
        //物料编码
        info.put(ConstUtil.MATCODE,paramData.get(ConstUtil.MATCODE));
        JSONObject result = ServiceUtil.requestService(info,session);

//        result = JSONObject.fromObject(TestData.material);
        try {
                    PrintWriter pw = response.getWriter();
            pw.write(result.toString());
        } catch (IOException e) {
            LogUtil.exceptionLog(e.getMessage(), e);
        }
    }

    /**
     * 通知轨道车放空车
     *
     * @param request
     * @param response
     * @param paramData
     */
    @RequestMapping(value = "/releaseTruck")
    @ControllerLog(remark = "放空车")
    public void releaseTruck(HttpServletRequest request, HttpServletResponse response, @RequestParam HashMap<String, Object> paramData) {

    }
}