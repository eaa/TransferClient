package com.yonyou.mm.controller;

import com.yonyou.mm.aop.LogUtil;
import com.yonyou.mm.service.ServiceUtil;
import com.yonyou.mm.util.ConstUtil;
import com.yonyou.mm.util.TestData;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

/**
 * Created by yijx on 2017/4/1.
 */
@Controller
public class TransferController {

    /**
     * 刷新备料申请列表，从后台查询数据
     * @param request
     * @param response
     * @param paramsData
     */
    @RequestMapping(value = "/refreshList")
    public void refresh(HttpServletRequest request, HttpServletResponse response, @RequestParam HashMap<String,Object> paramsData){
        HttpSession session = request.getSession();
        JSONObject info = new JSONObject();
        //添加参数
        //当前站点所在仓库
        info.put(ConstUtil.WAREHOUSE,session.getAttribute(ConstUtil.WAREHOUSE));
        //所在站点
        info.put(ConstUtil.POSITION,session.getAttribute(ConstUtil.POSITION));
        //类型
        info.put(ConstUtil.TASKTYPE,"queryReqPickm");

        //查询结果
        JSONObject result= ServiceUtil.requestService(info,session);

        JSONArray rows = JSONArray.fromObject(TestData.reqPickmList);
        result.put("rows",rows);
        PrintWriter pw = null;
        try {
            pw = response.getWriter();
            pw.write(result.toString());
        } catch (IOException e) {
            LogUtil.exceptionLog(e.getMessage(),e);
        }
    }

    /**
     * 发送到转库单界面的数据存入Session
     * @param request
     * @param response
     * @param paramsData
     */
    @RequestMapping(value = "/toTransBill")
    public void toTransBill(HttpServletRequest request, HttpServletResponse response, @RequestParam HashMap<String,Object> paramsData){
        HttpSession session = request.getSession();
        JSONObject info = JSONObject.fromObject(paramsData.get("info").toString());
        session.setAttribute("transInfo",info);
        JSONObject result = new JSONObject();
        result.put("flag",0);
        PrintWriter pw = null;
        try {
            pw = response.getWriter();
            pw.write(result.toString());
        } catch (IOException e) {
           LogUtil.exceptionLog(e.getMessage(),e);
        }

    }

    /**
     * 跳转到转库单界面
     * @param request
     * @param response
     * @param paramsData
     * @param model
     * @return
     */
    @RequestMapping(value = "/transBillPage")
    public String transBillPage(HttpServletRequest request, HttpServletResponse response, @RequestParam HashMap<String,Object> paramsData, Model model){
        HttpSession session = request.getSession();
        if(session.getAttribute("transInfo") !=null) {
            JSONObject info = JSONObject.fromObject(session.getAttribute("transInfo"));
            JSONArray rows = info.getJSONArray("rows");
            model.addAttribute("outwarehouse", session.getAttribute(ConstUtil.WAREHOUSENAME));
            model.addAttribute("outposition", session.getAttribute(ConstUtil.POSITIONNAME));
            model.addAttribute("inposition", info.get(ConstUtil.POSITION));
            model.addAttribute("inwarehouse", info.get("inwarehouse"));
            model.addAttribute("rows", rows);
            session.removeAttribute("transInfo");
            return "TransferBill";
        }
        return "WareHouse";
    }

    /**
     * 后台备料申请推生产领料推转库单，如果有选中多个备料申请，则多张转库单
     * @param request
     * @param response
     * @param paramsData
     */
    @RequestMapping(value = "/saveTransBill")
    public void saveTransBill(HttpServletRequest request, HttpServletResponse response, @RequestParam HashMap<String,Object> paramsData){
        HttpSession session = request.getSession();
        JSONObject info = new JSONObject();
        JSONArray  billRow =  JSONArray.fromObject(paramsData.get("info"));
        info.put(ConstUtil.ROWS,billRow);
        info.put(ConstUtil.TASKTYPE,"saveTrans");
        //发出仓库、站点
        info.put(ConstUtil.WAREHOUSE,session.getAttribute(ConstUtil.WAREHOUSE));
        info.put(ConstUtil.POSITION,session.getAttribute(ConstUtil.POSITION));

        JSONObject result = ServiceUtil.requestService(info,session);

//        result = new JSONObject();
//        JSONArray rows = JSONArray.fromObject(TestData.transBill);
//        result .put(ConstUtil.ROWS,rows);
        try {
            PrintWriter pw = response.getWriter();
            pw.write(result.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 查询货位
     * @param request
     * @param response
     */
    @RequestMapping(value = "/getAllocation")
    public void getAllocation(HttpServletRequest request, HttpServletResponse response){
        JSONObject allocation = JSONObject.fromObject(TestData.allocation);
        PrintWriter pw = null;
        try {
            pw = response.getWriter();
            pw.write(allocation.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 查询已发车的转库单
     * @param request
     * @param response
     */
    @RequestMapping(value = "/getDoneTransBill")
    public void getDoneTransBill(HttpServletRequest request, HttpServletResponse response){
        HttpSession session = request.getSession();
        JSONArray rows = JSONArray.fromObject(request.getParameter("rows"));
        JSONObject info = new JSONObject();
        info.put(ConstUtil.ROWS,rows);
        info.put(ConstUtil.TASKTYPE,"getDoneTransBill");

        JSONObject result=  ServiceUtil.requestService(info,session);
        JSONArray transBills = new JSONArray();
        if(result.containsKey(ConstUtil.ROWS) && result.get(ConstUtil.ROWS) instanceof JSONArray){
            transBills =result.getJSONArray(ConstUtil.ROWS);
        }

        try {
            PrintWriter pw = response.getWriter();
            pw.write(transBills.toString());
        } catch (IOException e) {
           LogUtil.exceptionLog(e.getMessage(),e);
        }
    }

    /**
     * 叫空车
     */
    @RequestMapping(value = "/reqTruck")
    public void reqTruck(){

    }
}
